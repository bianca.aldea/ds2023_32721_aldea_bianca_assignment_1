import React, { useEffect, useState } from 'react'; // Import useEffect hook
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import DeviceTable from "./components/DeviceTable";
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer, toast } from 'react-toastify';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import Card from 'react-bootstrap/Card';
import SockJsClient from 'react-stomp';

function Client() {
  const [tableColor, setTableColor] = useState('');


  useEffect(() => {
    const ws = new WebSocket("ws://localhost:8080/ws-message"); 

    ws.onmessage = (message) => {
      if (message.data === "S-a depasit bugetu") {
        toast("A fost depasit bugetul");
        setTableColor('red');
    }
    };

    return () => {
        ws.close();
    };
}, []);


 //parte websockets
 const [isMsg, setIsMsg] = useState(false);
 const [message, setMessage] = useState('');
 const [topics, setTopics] = useState([]);

 let onConnected = () => {
     console.log("Connected!!")
     setTopics(['/topic/message']);
   }
 
 let onDisconnect = () => {
     console.log("DISConnected!!")
 }

 let onMessageReceived = (msg) => {
     setMessage(msg)
     setIsMsg(true)

     //setTimeout
 }

  const handleLogout = () => {
    localStorage.removeItem('role');
    localStorage.removeItem('personId');
    window.location.href = '/'
  };
    const redirectToChat = () => {
      window.location.href = '/chat'; // Adresa URL către care vrei să redirecționezi
    };

  return (
    <div>
      <div>
        <ToastContainer autoClose={3000} />
        <div className="App">
          <button className="btn-logout" onClick={handleLogout}>
            <FontAwesomeIcon icon={faSignOutAlt} className="logout-icon" />
          </button>
          <button onClick={redirectToChat}>
      Go to Chat
    </button>
          { isMsg && < Card className='buget' style={{ width: '20rem',marginLeft:'40%',alignContent:'center', background: 'brown' }}>
            <div>
            <Card.Text style={{textAlign:"center",padding:"0px"}} >{message}</Card.Text>
            </div></Card>}
            <div className='device-table' style={{ backgroundColor: tableColor }}>
            <DeviceTable tableColor={tableColor} /> {/* Pass the color as a prop */}
          </div>
        </div>
      </div>
      <SockJsClient
            url={'http://localhost:8082/ws-message'}
            topics={topics}
            onConnect={onConnected}
            onDisconnect={onDisconnect}
            onMessage={msg => onMessageReceived(msg)}
            debug={false}
            />
    </div>
  );
}

export default Client;
