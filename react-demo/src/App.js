import './App.css';
import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom";
import { Login} from './Login';
import Administrator from './Administrator';
import Client from './Client';
import PrivateRoute from './PrivateRoute';
import ChatPage from './components/ChatPage';


function App() {
  return (
    <div>
      <Router>
        <Routes>
        <Route path="/chat" element={<ChatPage />} />
          <Route path="/" element={<Login />} />
          <Route
            path="/administrator"
            element={
              <PrivateRoute
                element={<Administrator />}
                allowedRoles={['administrator']}
              />
            }
          />
          <Route
            path="/client"
            element={
              <PrivateRoute element={<Client />} allowedRoles={['client']} />
            }
          />
        </Routes>
      </Router>
    </div>
  );
}


export default App;
