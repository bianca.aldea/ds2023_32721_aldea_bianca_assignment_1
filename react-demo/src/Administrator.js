import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import UserTable from "./components/UserTable";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer } from "react-toastify";
import Navbar from "./components/Navbar";
import DeviceTableAdmin from "./components/DeviceTableAdmin";
import GIF from "./components/homepage.gif";

function Administrator() {
  const [showUserTable, setShowUserTable] = useState(false);
  const [showDeviceTable, setShowDeviceTable] = useState(false);

  const handleShowUserTable = () => {
    setShowUserTable(true);
    setShowDeviceTable(false);
  };

  const handleShowDeviceTable = () => {
    setShowDeviceTable(true);
    setShowUserTable(false);
  };
  const redirectToChat = () => {
    window.location.href = '/chat'; // Adresa URL către care vrei să redirecționezi
  };


  return (
    <div>
      <div>
        <ToastContainer autoClose={3000} />
        <Navbar
          onShowUserTable={handleShowUserTable}
          onShowDeviceTable={handleShowDeviceTable}
        />
         <button onClick={redirectToChat}>
      Go to Chat
    </button>
        <div className="App">
          <div className="user-table" style={{ display: showUserTable ? "block" : "none" }}>
            <UserTable />
          </div>
          <div className="device-table" style={{ display: showDeviceTable ? "block" : "none" }}>
            <DeviceTableAdmin />
          </div>
          <div style={{ display: "flex", justifyContent: "center", alignItems: "center", height: "100vh" }}>
          <img src={GIF} alt="GIF" style={{ width: "1000px", height: "auto",  display: showDeviceTable || showUserTable ? "none" : "block"}} />
        </div>
        </div>
      </div>
    </div>
  );
}

export default Administrator;
