import React from 'react';
import { Navigate } from 'react-router-dom';

const PrivateRoute = ({ element, allowedRoles, fallbackPath = '/' }) => {
  const userRole = localStorage.getItem('role'); 

  if (allowedRoles.includes(userRole)) {
    return element;
  } else {
    return <Navigate to={fallbackPath} replace />;
  }
};

export default PrivateRoute;
