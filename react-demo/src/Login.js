import { React, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Button,Container, Col, Form,FormGroup,Row, Input } from 'reactstrap';
import axios from "axios";
import { HOST_PERSON, HOST_SECURITY } from "./Hosts";

const Login = () => {
const [username, setUsername] = useState("");
const [password, setPassword] = useState("");
const [errorMessage, setErrorMessage] = useState('');
const navigate = useNavigate();
    
const handleSubmit = async (event) => {
  event.preventDefault();
  try {
    const response = await axios.post(`${HOST_SECURITY}/login`, {
      "username": username,
      "password": password,
    });

    console.log("Login response:", response.data); // Debugging line

    if (response.status === 200) {
      var token = response.data.token;
      localStorage.setItem('jwtToken', token);

      const role = response.data.role;
      console.log("Role received:", role); // Debugging line

      // Check roles as per the backend response
      if (role.includes('ROLE_0')) { // Update this check as per your backend
        localStorage.setItem('role', 'administrator');
        navigate('/administrator');
      } else if (role.includes('ROLE_1')) { // Update this check as per your backend
        localStorage.setItem('role', 'client'); 
        navigate('/client');
      } else {
        setErrorMessage('Invalid role.');
      }
    } else {
      setErrorMessage('Log in failed.');
    }
    
    try {
      const personIdResponse = await axios.get(`${HOST_PERSON}/getPersonId/${username}`);
      const personId = personIdResponse.data;
      localStorage.setItem('personId', personId);
    } catch (error) {
      console.error('Failed to get ID:', error);
    }
  } catch (error) {
    if (error.response && (error.response.status === 400 || error.response.status === 401)) {
      setErrorMessage('Invalid username or password.');
    } else {
      console.error('An unknown error occurred:', error);
      navigate('/login');
    }
  }
};



return (
  <>
  <div style={{margin: '100px'}}></div>
  <Container className="my-5">
    <Row>
      <Col>
        <h1 className="display-5 text-center mb-5">Welcome!</h1>
      </Col>
    </Row>
    <Row className="justify-content-center">
      <Col md={4}>
        <Form onSubmit={handleSubmit} >
          <FormGroup>
            <Input
              type="text"
              name="username"
              id="username"
              placeholder="Username"
              bsSize="default" 
              className='mb-3'
              value={username}
              onChange={(event) => setUsername(event.target.value)}
            />
            
          </FormGroup>
          <FormGroup>
            <Input
              type="password"
              name="password"
              id="password"
              placeholder="Password"
              bsSize="default" 
              className='mb-5'
              value={password}
              onChange={(event) => setPassword(event.target.value)}
            />
          </FormGroup>

          <Row>
            <Col>
              <Button color="dark" block>Login</Button>
            </Col>
          </Row>
        
          {errorMessage && <div className="mt-5 mb-5 fw-bold error-message">{errorMessage}</div>}

        </Form>
      </Col>
    </Row>
  </Container>
  </>
);
};


export { Login };