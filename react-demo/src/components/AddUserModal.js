import React, { useState } from "react";
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { HOST_SECURITY } from "../Hosts";
import axios from "axios";
import './UserTable.css';

const AddUserModal = ({ onUserAdded }) => {
  const [showModal, setShowModal] = useState(false);
  const [user, setUser] = useState({
    username: "",
    password: "",
    role: ""
  });

  const getAuthHeaders = () => {
    var token = localStorage.getItem('jwtToken');
    const headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': `Bearer ${token}`

    };
    return headers;
  };
  const toggleModal = () => {
    setShowModal(!showModal);
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setUser({
      ...user,
      [name]: value,
    });
  };

  const handleSaveUser = async() => {
    const headers = getAuthHeaders();

    try {
      if (user.role === "admin") {
        user.role = 0;
      } else if (user.role === "client") {
        user.role = 1;
      }
       await axios.post(`${HOST_SECURITY}/auth/save`, {headers: headers}, {
        username: user.username,
        password: user.password,
        role: user.role
      });
      onUserAdded();
    } catch (error) {
      console.error(error);
    }
    setShowModal(false);
  };

  return (
    <>
     <button onClick={toggleModal} className="btn btn-dark add-button" >
       + New User
      </button>
      <Modal show={showModal} onHide={toggleModal}>
        <Modal.Header closeButton>
          <Modal.Title>Add User</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <div className="form-group">
              <label htmlFor="username">Username</label>
              <input
                type="text"
                className="form-control"
                id="username"
                name="username"
                value={user.username}
                onChange={handleInputChange}
              />
            </div>
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <input
                type="password"
                className="form-control"
                id="password"
                name="password"
                value={user.password}
                onChange={handleInputChange}
              />
            </div>
            <div className="form-group">
              <label htmlFor="role">Role</label>
              <select
                className="form-control"
                id="role"
                name="role"
                value={user.role}
                onChange={handleInputChange}
              >
                <option value="admin">Admin</option>
                <option value="client">Client</option>
              </select>
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={toggleModal}>
            Close
          </Button>
          <Button variant="primary" onClick={handleSaveUser}>
            Save User
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default AddUserModal;
