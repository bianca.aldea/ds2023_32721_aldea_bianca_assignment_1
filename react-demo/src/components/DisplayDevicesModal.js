import React, { useEffect, useState } from "react";
import Modal from 'react-bootstrap/Modal';
import axios from "axios";
import { toast } from 'react-toastify';
import { Card } from 'react-bootstrap';
import {HOST_SECURITYd } from "../Hosts";

const DisplayDevicesModal = ({ userId, onClose }) => {
  const [deviceData, setDeviceData] = useState([]);

  const getAuthHeaders = () => {
    const token = localStorage.getItem('jktToken');
    const headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': `Bearer ${token}`

    };
    return headers;
  };
  useEffect(() => {
    let isRendered = true;
    const headers = getAuthHeaders();

    axios.get(`${HOST_SECURITYd}/auth/getdevices/${userId}`, {headers})
      .then((response) => {
        if (isRendered) {
          setDeviceData(response.data);
          toast.success("Device data fetched successfully!");
        }
      })
      .catch((error) => {
        if (isRendered) {
          toast.error("Error fetching device data");
        }
      });

    return () => {
      isRendered = false;
    };
  }, [userId]);

  return (
    <Modal show={true} onHide={onClose}>
      <Modal.Header closeButton>
        <Modal.Title>User's Devices</Modal.Title>
      </Modal.Header>
      <Modal.Body >
        <div>
          {deviceData.map((device, index) => (
            <Card className="w-50 mx-auto" key={index}>
              <Card.Body>
                <Card.Title>Device #{device.id}</Card.Title>
                <Card.Text>
                  <strong>Description:</strong> {device.description}
                  <br />
                  <strong>Address:</strong> {device.address}
                  <br />
                  <strong>Maximum hourly energy consumption:</strong> {device.maxconsumption}
                </Card.Text>
              </Card.Body>
            </Card>
          ))}
        </div>
      </Modal.Body>
    </Modal>
  );
};

export default DisplayDevicesModal;
