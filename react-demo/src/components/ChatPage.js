import React, { useState, useEffect, useRef } from 'react';
import SockJsClient from 'react-stomp';
import '../App.css';
import { Button } from 'react-bootstrap';
import { Form } from 'react-bootstrap';
import './ChatPage.css';


const Chat = () => {
    const [messages, setMessages] = useState([]);
    const [messageInput, setMessageInput] = useState('');
    const [Id, setId] = useState('');
    const [topics, setTopics] = useState([]);
    const [isTyping, setIsTyping] = useState(false);


    // ca sa putem trimite mesaje, trebe sa facem o referinta catre obiectul de client socket, pe care o initializam cu null la inceput
    //clientRef este un obiect de referință (Ref) 
    //creat folosind useRef hook din React
    //Aceasta este o modalitate de a păstra o referință către un element sau un obiect în cadrul componentelor funcționale în React, 
    //care poate fi utilizată pentru a accesa și manipula acest element sau obiect.
    const clientRef = useRef(null);


    useEffect(() => {
         // Utilizarea useEffect pentru a seta username-ul când componenta este randata
        setId(localStorage.getItem('personId'));

         // Funcția returnată va fi apelată când componenta este dezactivată, inchidem conexiunea
        return () => {
            if (clientRef.current && clientRef.current.deactivate) {
                clientRef.current.deactivate();
            }
        };
    }, []);

     // Funcție pentru a gestiona schimbarea valorii în input-ul de mesaj
   const handleMessageChange = (e) => {
    const input = e.target.value;
    setMessageInput(input);
    setIsTyping(input.length > 0);
};


      // Funcție pentru a trimite mesajul
    const handleSendMessage = () => {
        //aici voi schimbati, va mai adaugati ce va mai trebuie, eu am lasat reciever random
        const message = {
            sender: Id,
            receiver: 'someone',
            content: messageInput,
        };
        //current este proprietatea din obiectul de referință care conține referința efectivă către elementul sau obiectul 
        //pe care l-ați legat cu useRef. 
        //Prin accesarea proprietății current, puteți interacționa cu obiectul sau elementul din afara ciclului de viață al componentei.
        //practic un mod de a accesa obiectul in sine, nu doar referinta

        //sendMessage este o metodă specifică a acestui client WebSocket (SockJsClient) 
        //care este utilizată pentru a trimite mesaje către serverul WebSocket
        if (clientRef.current && clientRef.current.sendMessage) {
            clientRef.current.sendMessage('/app/chat', JSON.stringify(message));
        }

        //golim mesajul dupa ce l-am trimis
        setMessageInput('');
        setIsTyping(false);

    };

    // cand primim mesaj, pe langa cele pe care le avem deja vrem sa le pastram
    //de asta punem cu .. inainte, asta inseamna ca la array-ul de mesaje deja existent, mai adaugam pe cel pe care l-am primit
    const onMessageReceived = (msg) => {
        setMessages([...messages, msg]);
    };

    
    //setam topicul aici ca sa evitam sa ne dea eroare de connection cannot be established yet
    let onConnected = () => {
      setTopics(['/topic/messages'])
      console.log("Connected!!")
    }

    return (
        <div className="chat-container">
            <div className="chat-header">
                <h3>Chat Room</h3>
            </div>
            <div className="message-container">
                {messages.map((msg, index) => (
                    <div key={index} className={`message ${msg.sender === Id ? 'message-sent' : 'message-received'}`}>
                        <div className="message-content">
                            <span className="message-sender">{msg.sender}:</span>
                            <p>{msg.content}</p>
                        </div>
                    </div>
                ))}
                {isTyping && <div className="typing-notification">Someone is typing...</div>}
            </div>
            <Form>
                <Form.Control
                                type="text"
                                value={messageInput}
                                onChange={handleMessageChange}
                                placeholder="Type your message..."
                                style={{ marginTop: "5px" }}
                                className="bg-dark text-light" 
                            />
                        </Form>
                        <Button style={{ marginTop: "5px" }} variant='dark' onClick={handleSendMessage}>Send</Button>
                        <SockJsClient
                            url="http://localhost:8083/ws"
                            topics={topics}
                            onConnect={onConnected}
                            onMessage={onMessageReceived}
                            ref={(client) => {
                                if (client) {
                                    clientRef.current = client;
                                }
                            }}
                        />
                    </div>
                );
            };

 export default Chat;
            
            
