import React from 'react';

const MeasurementsTable = ({ measurements }) => (
  <table className="table">
    <thead>
      <tr>
        <th>ID</th>
        <th>Timestamp</th>
        <th>Value (kWh)</th>
        <th>Sum (kWh)</th>
        <th>Device ID</th>
      </tr>
    </thead>
    <tbody>
      {measurements.map((measurement) => (
        <tr key={measurement.idmeasurements}>
          <td>{measurement.idmeasurements}</td>
          <td>{new Date(measurement.timest).toLocaleString()}</td>
          <td>{measurement.val}</td>
          <td>{measurement.sum}</td>
          <td>{measurement.deviceId}</td>
        </tr>
      ))}
    </tbody>
  </table>
);

export default MeasurementsTable;
