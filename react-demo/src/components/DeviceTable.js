import React, { useEffect, useState } from "react";
import axios from "axios";
import { Card } from 'react-bootstrap'; 
import { HOST_SECURITYd } from "../Hosts";
import { toast } from 'react-toastify';
import './DeviceTable.css';
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

const DeviceTable = ({ tableColor }) => {
  const [deviceData, setDeviceData] = useState([]);
  const [personId, setPersonId] = useState("");
  const [selectedDeviceId, setSelectedDeviceId] = useState(null);
  const [selectedDate, setSelectedDate] = useState(new Date());

  const getAuthHeaders = () => {
    const token = localStorage.getItem('jwtToken');
    const headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': `Bearer ${token}`
    };
    return headers;
  };

  useEffect(() => {
    const storedPersonId = localStorage.getItem('personId');
    setPersonId(storedPersonId);
  }, []);

  useEffect(() => {
    let isRendered = true;
    const headers = getAuthHeaders();
    axios.get(`${HOST_SECURITYd}/auth/getdevices/${personId}`, {headers: headers})
      .then((response) => {
        if (isRendered) {
          setDeviceData(response.data);
          toast.success("Device data fetched successfully!");
        }
      })
      .catch((error) => {
        if (isRendered) {
          toast.error("Error fetching device data");
        }
      });
  
    return () => {
      isRendered = false;
    };
  }, [personId]);
  

  const cardStyle = {
    backgroundColor: tableColor === 'red' ? '#ffcccc' : "", 
  };

  const handleViewCharts = (deviceId) => {

    if (selectedDeviceId === deviceId) {
      setSelectedDeviceId(null); 
    } else {
      setSelectedDeviceId(deviceId); // Select the new device
    };
  };  

  return (
    <div className="card-container">
      {deviceData.map((device, index) => (
        <Card key={device.id} className="device-card" style={cardStyle}>
          <Card.Body>
            <button style={{ backgroundColor: 'green', color: 'white', padding: '10px 15px', border: 'none'}} onClick={() => handleViewCharts(device.id)}>View Charts</button>
            <Card.Title>Device #{device.id}</Card.Title>
            <Card.Text>
              <strong>Description:</strong> {device.description}
              <br />
              <strong>Address:</strong> {device.address}
              <br />
              <strong>Maximum hourly energy consumption:</strong> {device.maxconsumption}
            </Card.Text>
          </Card.Body>
        </Card>
      ))}
      <DatePicker
        selected={selectedDate}
        onChange={(date) => setSelectedDate(date)}
        dateFormat="yyyy-MM-dd"
        inline
      />
    </div>
  );
}  

export default DeviceTable;
