import React, { useEffect, useState } from "react";
import axios from "axios";
import {Table} from 'react-bootstrap'
import { HOST_SECURITY } from "../Hosts";
import { HOST_DEVICE } from "../Hosts";
import {  toast } from 'react-toastify';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash, faEdit, faTabletAlt, faRedo } from '@fortawesome/free-solid-svg-icons';
import EditUserModal from './EditUserModal';
import './UserTable.css';
import AddUserModal from "./AddUserModal";
import DisplayDevicesModal from "./DisplayDevicesModal.js";


const UserTable = () => {
  const [userData, setUserData] = useState([]);
  const [editingUser, setEditingUser] = useState(null);
  const [displayingUserId, setDisplayingUserId] = useState(null);
  
  const getAuthHeaders = () => {
    //return headers;
  };

  useEffect(() => {
    let isRendered = true;

    var token = localStorage.getItem('jwtToken');
    const headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': `Bearer ${token}`

    };
    axios.get(`${HOST_SECURITY}/auth/allper`,  {"headers":headers} )
      .then((response) => {
        if (isRendered) {
          setUserData(response.data);
          toast.success("User data fetched successfully!");
        }
      })
      .catch((error) => {
        if (isRendered) {
          toast.error("Error fetching user data");
        }
      });

    return () => {
      isRendered = false;
    };
  }, []);

  function mapRole(role) {
    return role === 0 ? "ADMIN" : "CLIENT";
  }

  const handleDeleteUser = async (id) => {
    const headers = getAuthHeaders();

    try {
      await axios.delete(`${HOST_SECURITY}/auth/deleteById?id=${id}`, { headers });
  
      const devicesResponse = await axios.get(`${HOST_DEVICE}/getdevices/${id}`);
      const devices = devicesResponse.data;
  
      await Promise.all(devices.map(async (device) => {
        await axios.delete(`${HOST_DEVICE}/deleteById?id=${device.id}`);
      }));
  
      setUserData(userData.filter((user) => user.id !== id));
    } catch (error) {
      console.error(error);
    }
  };
  

  const handleEditUser = (user) => {
    setEditingUser(user);
  };

  const handleSaveEditedUser = (editedUser) => {
    setUserData((prevUserData) =>
      prevUserData.map((user) =>
        user.id === editedUser.id ? editedUser : user
      )
    );
  };

  const handleDisplayDevices = (userId) => {
    setDisplayingUserId(userId);
  };

  const refreshTable = async () => {
    try {
      const response = await axios.get(`${HOST_SECURITY}/auth/allper`);
      if (response.data) {
        setUserData(response.data);
        toast.success("User data refreshed successfully!");
      }
    } catch (error) {
      toast.error("Error refreshing user data");
    }
  };
  
  
  
  return (
    <div className="table-container">
    <Table className="table table-striped table-bordered table-hover" style={{marginBottom: 0 }}>
        <thead>
            <AddUserModal />
          <tr>
            <th>#id</th>
            <th>Username</th>
            <th>Password</th>
            <th>Role</th>
            <th></th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {userData.map((user, index) => (
            <tr key={index}>
              <td>{user.id}</td>
              <td>{user.username}</td>
              <td>{user.password}</td>
              <td>{mapRole(user.role)}</td>
              <td>
                <button
                  onClick={() => handleDeleteUser(user.id)}
                  className="btn"
                  style={{ marginLeft: "25%" }}
                >
                  <FontAwesomeIcon icon={faTrash} />
                </button>
              </td>
              <td>
                <button
                  onClick={() => handleEditUser(user)}
                  className="btn"
                  style={{ marginLeft: "25%" }}
                >
                  <FontAwesomeIcon icon={faEdit} />
                </button>
              </td>
              <td>
                <button
                  onClick={() => handleDisplayDevices(user.id)}
                  className="btn"
                  style={{ marginLeft: "25%" }}
                >
                  <FontAwesomeIcon icon={faTabletAlt} />
                </button>
              </td>
            </tr>
          ))}
        </tbody>
        <EditUserModal
          user={editingUser}
          onSave={handleSaveEditedUser}
          onClose={() => setEditingUser(null)}
        />
         {displayingUserId && (
        <DisplayDevicesModal
          userId={displayingUserId}
          onClose={() => setDisplayingUserId(null)}
        />
      )}
      </Table>
      <button onClick={refreshTable} className="btn btn-dark" style={{marginTop: 0 }}> 
        <FontAwesomeIcon icon={faRedo} /> Refresh Table
      </button>
    </div>
  );
};

export default UserTable;