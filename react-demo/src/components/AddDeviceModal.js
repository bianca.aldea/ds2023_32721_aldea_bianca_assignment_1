import React, { useState } from "react";
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { HOST_SECURITYd } from "../Hosts";
import { HOST_SECURITY} from "../Hosts";
import axios from "axios";
import './DeviceTable.css';

const AddDeviceModal = () => {
  const [showModal, setShowModal] = useState(false);
  const [device, setDevice] = useState({
    description: "",
    address: "",
    maxconsumption: 0,
    personId: 0
  });
  const [error, setError] = useState("");

  const toggleModal = () => {
    setShowModal(!showModal);
    setError("");
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setDevice({
      ...device,
      [name]: value,
    });
  };

  const getAuthHeaders = () => {
    const token = localStorage.getItem('jktToken');
    const headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': `Bearer ${token}`

    };
    return headers;
  };

  const handleSaveDevice = async() => {
    try {
      const headers = getAuthHeaders();
      const personResponse = await axios.get(`${HOST_SECURITY}/getById?id=${device.personId}`, {headers});
      if (personResponse.data) {
        if (personResponse.data.role === 1) {
        await axios.post(`${HOST_SECURITYd}/save`, {
          description: device.description,
          address: device.address,
          maxconsumption: device.maxconsumption,
          personId: device.personId,
        });
        setShowModal(false);
      } 
      else {
        setError("Person with the provided ID is admin"); 
      }
    } else {
        setError("Person with the provided ID not found.");
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <>
     <button onClick={toggleModal} className="btn btn-dark add-button" >
       + New Device
      </button>
      <Modal show={showModal} onHide={toggleModal}>
        <Modal.Header closeButton>
          <Modal.Title>Add Device</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <div className="form-group">
              <label htmlFor="description">Description</label>
              <input
                type="text"
                className="form-control"
                id="description"
                name="description"
                value={device.description}
                onChange={handleInputChange}
              />
            </div>
            <div className="form-group">
              <label htmlFor="address">Address</label>
              <input
                type="text"
                className="form-control"
                id="address"
                name="address"
                value={device.address}
                onChange={handleInputChange}
              />
            </div>
            <div className="form-group">
              <label htmlFor="maxconsumption">Max Consumption</label>
              <input
                type="number"
                className="form-control"
                id="maxconsumption"
                name="maxconsumption"
                value={device.maxconsumption}
                onChange={handleInputChange}
              />
            </div>
            <div className="form-group">
              <label htmlFor="personId"> Person Id</label>
              <input
                className="form-control"
                id="personId"
                name="personId"
                value={device.personId}
                onChange={handleInputChange}
              />
              </div>
          </form>
          {error && <p className="text-danger">{error}</p>}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={toggleModal}>
            Close
          </Button>
          <Button variant="primary" onClick={handleSaveDevice}>
            Save Device
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default AddDeviceModal;
