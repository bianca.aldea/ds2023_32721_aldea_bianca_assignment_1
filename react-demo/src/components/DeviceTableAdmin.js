import React, { useEffect, useState } from "react";
import axios from "axios";
import {HOST_SECURITYd } from "../Hosts";
import { toast } from 'react-toastify';
import './UserTable.css';
import { Table } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash, faEdit, faRedo } from '@fortawesome/free-solid-svg-icons'; 
import EditDeviceModal from './EditDeviceModal';
import AddDeviceModal from "./AddDeviceModal";

const DeviceTableAdmin = () => {
  const [deviceData, setDeviceData] = useState([]);
  const [editingDevice, setEditingDevice] = useState(null);

  const getAuthHeaders = () => {
    var token = localStorage.getItem('jwtToken');
    const headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': `Bearer ${token}`

    };
    return headers;
  };
  const headers = getAuthHeaders();

  const refreshTable = () => {
  axios.get(`${HOST_SECURITYd}/auth/all`, { headers: headers })
    .then((response) => {
      setDeviceData(response.data);
      toast.success("Device data refreshed successfully!");
    })
    .catch((error) => {
      toast.error("Error refreshing device data");
    });
};

useEffect(() => {
  refreshTable();
}, [refreshTable]);


  const handleDeleteDevice = async (id) => {
    try {
      axios.delete(`${HOST_SECURITYd}/deleteById?id=${id}`, {headers});
      setDeviceData(deviceData.filter((device) => device.id !== id));
    } catch (error) {
      console.error(error);
    }
  };

  const handleEditDevice = (device) => {
    setEditingDevice(device);
  };

  const handleSaveEditedDevice = (editedDevice) => {
    setDeviceData((prevDeviceData) =>
      prevDeviceData.map((device) =>
        device.id === editedDevice.id ? editedDevice : device
      )
    );
  };

  return (
    <div className="table-container">
      <Table className="table table-sm table-striped table-bordered table-hover">
        <thead>
          <AddDeviceModal />
          <tr>
            <th>#id</th>
            <th>Description</th>
            <th>Address</th>
            <th>Max hourly energy consumption</th>
            <th>Person Id</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {deviceData.map((device, index) => (
            <tr key={index}>
              <td>{device.id}</td>
              <td>{device.description}</td>
              <td>{device.address}</td>
              <td>{device.maxconsumption}</td>
              <td>{device.personId}</td>
              <td>
                <button onClick={() => handleDeleteDevice(device.id)} className="btn">
                  <FontAwesomeIcon icon={faTrash} />
                </button>
                <button onClick={() => handleEditDevice(device)} className="btn">
                  <FontAwesomeIcon icon={faEdit} />
                </button>
              </td>
            </tr>
          ))}
        </tbody>
        <EditDeviceModal device={editingDevice} onSave={handleSaveEditedDevice} onClose={() => setEditingDevice(null)} />
      </Table>
      <button onClick={refreshTable} className="btn btn-dark"> 
        <FontAwesomeIcon icon={faRedo} /> Refresh Table
      </button>
    </div>
  );
};

export default DeviceTableAdmin;
