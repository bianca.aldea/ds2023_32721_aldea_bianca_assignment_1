import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignOutAlt  } from '@fortawesome/free-solid-svg-icons';


const transparentButtonStyle = {
    backgroundColor: "transparent",
    border: "none",
    cursor: "pointer",
    outline: "none",
    color: "grey"
  };
  const handleLogout = () => {
    localStorage.removeItem('role');
    localStorage.removeItem('personId');
    window.location.href = '/'
  };


  function Navbar({ onShowUserTable, onShowDeviceTable }) {
    return (
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark p-3">
      <div class="container-fluid">
      <a class="navbar-brand" href="/Administrator"> Energy Management System</a>
        <button style={transparentButtonStyle} onClick={onShowUserTable}>Show Users</button>
        <button style={transparentButtonStyle} onClick={onShowDeviceTable}>Show Devices</button>
        <ul className="navbar-nav ml-auto">
          <li className="nav-item">
            <button className="btn-logout" onClick= { handleLogout }>
              <FontAwesomeIcon icon={faSignOutAlt} className="logout-icon" />
            </button>
          </li>
        </ul>
        </div>
      </nav>
    );
  }
  
  export default Navbar;
  


