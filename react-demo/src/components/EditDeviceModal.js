import React, { useState, useEffect } from "react";
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import {HOST_SECURITYd } from "../Hosts";
import axios from "axios";

const EditDeviceModal = ({ device, onSave, onClose }) => {
  const [editedDevice, setEditedDevice] = useState({ ...device });

  useEffect(() => {
    if (device) {
      setEditedDevice(device);
    }
  }, [device]);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setEditedDevice({
      ...editedDevice,
      [name]: value,
    });
  };

  const handleSaveDevice = async () => {
    try {
        axios.post(`${HOST_SECURITYd}/auth/edit/${editedDevice.id}`, {
        description: editedDevice.description,
        address: editedDevice.address,
        maxconsumption: editedDevice.maxconsumption,
      });
      onSave(editedDevice);
    } catch (error) {
      console.error(error);
    }
    onClose();
  };

  return (
    device && (
      <Modal show={true} onHide={onClose}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Device</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <div className="form-group">
              <label htmlFor="description">Description</label>
              <input
                type="text"
                className="form-control"
                id="description"
                name="description"
                value={editedDevice.description}
                onChange={handleInputChange}
              />
            </div>
            <div className="form-group">
              <label htmlFor="address">Address</label>
              <input
                type="text"
                className="form-control"
                id="address"
                name="address"
                value={editedDevice.address}
                onChange={handleInputChange}
              />
            </div>
            <div className="form-group">
              <label htmlFor="maxconsumption">Maximum Consumption</label>
              <input
                type="text"
                className="form-control"
                id="maxconsumption"
                name="maxconsumption"
                value={editedDevice.maxconsumption}
                onChange={handleInputChange}
              />
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={onClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleSaveDevice}>
            Save Device
          </Button>
        </Modal.Footer>
      </Modal>
    )
  );
};

export default EditDeviceModal;
