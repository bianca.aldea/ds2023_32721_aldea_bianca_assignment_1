package ro.tuc.ds2020;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import ro.tuc.ds2020.entities.Measurement;
import ro.tuc.ds2020.services.MeasurementService;
import ro.tuc.ds2020.websocket.WebSocketController;

import java.util.Calendar;
import java.util.Date;

@SpringBootApplication
@EnableScheduling
public class Ds2020Application extends SpringBootServletInitializer {

    private static volatile double actualMHEC = 0.0;

    public static void main(String[] args) {
        SpringApplication.run(Ds2020Application.class, args);
    }

    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    WebSocketController webSocketTextController;
    @Autowired
    MeasurementService measurementService;

    @RabbitListener(queues = "senzoriii")
    public void run(String msg1) throws Exception {
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
        System.out.println(msg1); // Afișează direct mesajul primit din coadă

        // Deserialize JSON string to a JSON object
        ObjectNode jsonNode = objectMapper.readValue(msg1, ObjectNode.class);

        // Create a new Measurement object
        Measurement measurement = new Measurement();


        // Parse and set the 'val' field from 'data'
        if (jsonNode.hasNonNull("data")) {
            String dataString = jsonNode.get("data").asText().replace(".", "");
            Double val = Double.parseDouble(dataString);
            System.out.println("Parsed 'val' from JSON: " + val);


            measurement.setVal(val);
            Measurement latestHourMeasurement = measurementService.findLatestMeasurementWithinHour(new Date());
            if (latestHourMeasurement != null && latestHourMeasurement.getSum() != null) {
                measurement.setSum(latestHourMeasurement.getSum() + val);
            } else {
                measurement.setSum(val); // If there's no measurement in the current hour, start a new sum
            }
        }


        if (jsonNode.hasNonNull("device_id")) {
            String deviceId = jsonNode.get("device_id").asText();
            measurement.setDeviceId(deviceId);
        }

        // Parse and set the 'timest' field from 'timestamp'
        if (jsonNode.hasNonNull("timestamp")) {
            Long timestamp = jsonNode.get("timestamp").asLong();
            Date date = new Date(timestamp);
            measurement.setTimest(date);
        }


        Measurement latestHourMeasurement = measurementService.findLatestMeasurementWithinHour(new Date());
        if (latestHourMeasurement != null && isSameHour(latestHourMeasurement.getTimest(), new Date())) {
            // If the latest measurement is within the same hour, accumulate the sum
            measurement.setSum(latestHourMeasurement.getSum() + measurement.getVal());
        } else {
            // If a new hour has started, reset sum and val to 0
            measurement.setVal(0.0);
            measurement.setSum(0.0);
        }

        measurementService.saveMeasurement(measurement);
        // Business logic can go here
        if (measurement.getSum() > actualMHEC){
            System.out.println("Buget depasit");
            webSocketTextController.sendMessage("The device is above the maximum limit!");
        } else {
            System.out.println("Se incadreaza in buget");
        }
    }
    private boolean isSameHour(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);
        return cal1.get(Calendar.HOUR_OF_DAY) == cal2.get(Calendar.HOUR_OF_DAY);
    }
    @RabbitListener(queues = "notification")
    public void processNotificationQueue(String message) throws Exception {
        System.out.println(" [x] Received notification message: " + message);
        ObjectNode jsonNode = objectMapper.readValue(message, ObjectNode.class);

        if (jsonNode.hasNonNull("actualMHEC")) {
            actualMHEC = jsonNode.get("actualMHEC").asDouble();
            webSocketTextController.sendMessage("Updated 'actualMHEC' value from notifications queue: " + actualMHEC);
        }
    }

    @Scheduled(cron = "0 0 * * * *") // Runs on the zero minute of every hour
    public void resetMeasurementValues() {
        Measurement latestHourMeasurement = measurementService.findLatestMeasurementWithinHour(new Date());
        if (latestHourMeasurement != null) {
            latestHourMeasurement.setVal(0.0);
            latestHourMeasurement.setSum(0.0);
            measurementService.saveMeasurement(latestHourMeasurement);
            // Optionally notify through WebSocket if needed
            System.out.println("Values have been reset for the new hour.");
        }
    }
}