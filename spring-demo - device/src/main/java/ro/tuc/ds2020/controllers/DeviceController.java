package ro.tuc.ds2020.controllers;

import com.rabbitmq.client.Channel;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.services.DeviceService;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;

import com.rabbitmq.client.Channel;




@RestController
@RequestMapping(value="/device")
public class DeviceController {
    @Autowired
    DeviceService deviceService;

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping(method = RequestMethod.GET, value = "/all")
    @ResponseBody
    public List<Device> getAll() {
        return deviceService.getAll();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getdevices/{personId}")
    @ResponseBody
    public List<Device> getByPersonId(@PathVariable("personId") int personId) {
        return this.deviceService.getByPersonId(personId);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getById")
    @ResponseBody
    public Device getById(@RequestParam(name = "id") Integer id) {
        return deviceService.getById(id);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/deleteById")
    @ResponseBody
    public ResponseEntity<String> deleteDeviceById(@RequestParam(name = "id") Integer id) throws NotFoundException {
        deviceService.deleteById(id);
        return ResponseEntity.ok("Dispozitivul a fost șters.");
    }
    @RequestMapping(method = RequestMethod.DELETE, value = "/deleteByPersonId/{personId}")
    @ResponseBody
    public ResponseEntity<String> deleteDevicesByPersonId(@PathVariable("personId") int personId) {
        deviceService.deleteDevicesByPersonId(personId);
        return ResponseEntity.ok("Toate dispozitivele au fost șterse pentru persoana cu ID-ul " + personId + ".");
    }


    @RequestMapping(method= RequestMethod.POST, value ="/save")
    @ResponseBody
    public Device saveDevice(@RequestBody Device device) throws IOException {
        return deviceService.saveDevice(device);
    }
//    @RequestMapping(method = RequestMethod.POST, value = "/edit/{id}")
//    @ResponseBody
//    public ResponseEntity<String> editDevice(@PathVariable Integer id, @RequestBody Device updatedDevice) throws NotFoundException {
//        Device editedDevice = deviceService.editDevice(id, updatedDevice);
//        if (editedDevice != null) {
//            return ResponseEntity.ok("Dispozitivul a fost actualizat.");
//        } else {
//            return ResponseEntity.notFound().build();
//        }
//    }

    @RequestMapping(method = RequestMethod.POST, value = "/edit/{id}")
    @ResponseBody
    public ResponseEntity<String> editDevice(@PathVariable Integer id, @RequestBody Device updatedDevice) throws NotFoundException {
        Device editedDevice = deviceService.editDevice(id, updatedDevice);

        String actualMHEC = editedDevice.getMhec();
        // After successful update, send the message to RabbitMQ
        try {
            // Configure the connection to RabbitMQ
            ConnectionFactory factory = new ConnectionFactory();
            factory.setUri("amqps://qqrckpte:XJaGGNrsmSydPVpYwZishlWLKf48xUYf@moose.rmq.cloudamqp.com/qqrckpte");

            // Create a new connection and a channel
            try (Connection connection = (Connection) factory.newConnection(); Channel channel = connection.createChannel()) {
                // The queue name
                String queueName = "notification"; // Replace with your actual queue name

                // Declare the queue (if it's not already declared)
                channel.queueDeclare(queueName, true, false, false, null);

                // The simple message to send
                String message = String.format("{\"deviceId\": \"%d\", \"actualMHEC\": \"%s\"}", editedDevice.getId(), actualMHEC);

                // Publish the message to the RabbitMQ queue
                channel.basicPublish("", queueName, null, message.getBytes(StandardCharsets.UTF_8));
                System.out.println("Updated device and actualMHEC sent to queue: " + queueName);
            } catch (IOException | TimeoutException e) {
                // Handle errors related to connection or sending
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to send message to RabbitMQ.");
            }
        } catch (URISyntaxException | NoSuchAlgorithmException | KeyManagementException e) {
            // Handle other exceptions
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error configuring RabbitMQ connection.");
        }

        // Return a response indicating success
        System.out.println("Device with ID " + id + " has been successfully updated.");
        return ResponseEntity.ok("Device updated successfully and MHEC sent to queue.");

    }

}